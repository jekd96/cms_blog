import org.cms.model.dao.security.AuthorityDAO;
import org.cms.model.entity.security.Authority;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 23.06.2014.
 */

@ContextConfiguration(locations = {"classpath:/spring-model.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorityDAOTest {

    @Autowired
    AuthorityDAO authorityDAO;

    @Test
    public void testAuthorityDAO(){

        for(Authority authority : authorityDAO.list()){
            System.out.println(authority.getAuthority());
        }
    }
}
