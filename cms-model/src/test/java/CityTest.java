import org.cms.model.dao.CityDAO;
import org.cms.model.entity.City;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 22.06.2014.
 */

@ContextConfiguration(locations = {"classpath:/spring-model.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class CityTest {

    @Autowired
    CityDAO cityDAO;


    @Test
    public void testCityDAO(){
        int i = 0;

        for(City city : cityDAO.list() ){
            System.out.println(city.getNameCity());
        }
    }

}
