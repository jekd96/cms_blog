import org.cms.model.dao.security.UserDAO;
import org.cms.model.entity.security.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:/spring-model.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserTest {

    @Autowired
    private UserDAO userDAO;

    @Test
    public void someTest(){

        //Тест создания нового пользователя, и добавление в БД

        for(User u : userDAO.list()){
            System.out.println(u.getUsername());
        }

    }

}
