import org.cms.model.dao.CountryDAO;
import org.cms.model.entity.Country;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 23.06.2014.
 */

@ContextConfiguration(locations = {"classpath:/spring-model.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class CountryDAOTest {

    @Autowired
    CountryDAO countryDAO;

    @Test
    public void testCountryDAO(){

        for(Country country : countryDAO.list()){
            System.out.println(country.getNameCountry());
        }

    }
}
