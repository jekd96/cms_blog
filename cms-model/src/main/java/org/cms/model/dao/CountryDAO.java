package org.cms.model.dao;

import org.cms.model.entity.Country;

/**
 * Created by freeman on 23.06.2014.
 */
public interface CountryDAO extends CommonDAO<Country> {
}
