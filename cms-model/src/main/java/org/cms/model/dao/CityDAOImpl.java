package org.cms.model.dao;

import org.cms.model.entity.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 22.06.2014.
 */
@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CityDAOImpl extends CommonDAOImpl<City> implements CityDAO {

    @Autowired
    public CityDAOImpl(City entity){
        super(entity);
    }

}
