package org.cms.model.dao;

import org.cms.model.entity.City;

/**
 * Created by freeman on 22.06.2014.
 */
public interface CityDAO extends CommonDAO<City> {
}
