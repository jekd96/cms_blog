package org.cms.model.dao;

import org.hibernate.Criteria;

import java.io.Serializable;
import java.util.List;

public interface CommonDAO<T> {

    T add(T bean);

    Criteria getCriteria();

    T update(T bean);

    void delete(T bean);

    void delete(Long id);

    T findById(Serializable id);

    T loadById(Serializable id);


    T createEntity();

    List<T> list();

    List<T> list(int offset, int limit);

    int count();
}
