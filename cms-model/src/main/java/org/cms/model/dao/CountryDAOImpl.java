package org.cms.model.dao;

import org.cms.model.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 23.06.2014.
 */

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CountryDAOImpl extends CommonDAOImpl<Country> implements CountryDAO {

    @Autowired
    public CountryDAOImpl(Country entity){
        super(entity);
    }

}
