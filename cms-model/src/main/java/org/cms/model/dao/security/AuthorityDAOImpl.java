package org.cms.model.dao.security;

import org.cms.model.dao.CommonDAOImpl;
import org.cms.model.entity.security.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 23.06.2014.
 */
@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class AuthorityDAOImpl extends CommonDAOImpl<Authority> implements AuthorityDAO {

    @Autowired
    public AuthorityDAOImpl(Authority entity){
        super(entity);
    }
}
