package org.cms.model.dao;

import org.cms.model.entity.Comment;

/**
 * Created by freeman on 23.06.2014.
 */
public interface CommentDAO extends CommonDAO<Comment> {
}
