package org.cms.model.dao;

import org.cms.model.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by freeman on 23.06.2014.
 */

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CommentDAOImpl extends CommonDAOImpl<Comment> implements CommentDAO {

    @Autowired
    public CommentDAOImpl(Comment entity){
        super(entity);
    }

}
