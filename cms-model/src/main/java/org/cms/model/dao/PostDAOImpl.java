package org.cms.model.dao;

import org.cms.model.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;

import javax.transaction.Transactional;

@Repository
@org.springframework.transaction.annotation.Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class PostDAOImpl extends CommonDAOImpl<Post> implements PostDAO {

    @Autowired
    public PostDAOImpl(Post post){
        super(post);
    }

}
