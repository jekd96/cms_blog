package org.cms.model.dao.security;


import org.cms.model.entity.security.User;

public interface UserDAO extends SecurityDAO<User> {
}
