package org.cms.model.dao;

import org.cms.model.entity.Post;

public interface PostDAO extends CommonDAO<Post> {
}
