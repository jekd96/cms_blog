package org.cms.model.dao;

import org.cms.model.entity.CommonEntity;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CommonDAOImpl<T extends CommonEntity> implements CommonDAO<T> {

    @Autowired
    protected SessionFactory sessionFactory;

    protected CommonDAOImpl() {
    }

    protected CommonDAOImpl(T entity) {
        this.entity = entity;
    }

    public T entity;

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public T add(T bean) {
        sessionFactory.getCurrentSession().saveOrUpdate(bean);
        return bean;
    }

    @Override
    public Criteria getCriteria(){
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entity.getClass());
        criteria.setCacheable(true);
        return criteria;
    }

    @Override
    public T update(T bean) {
        sessionFactory.getCurrentSession().saveOrUpdate(bean);
        return bean;
    }

    @Override
    public void delete(T bean) {
        sessionFactory.getCurrentSession().delete(bean);
    }

    @Override
    public void delete(Long id) {
        String deleteById  = "delete from "+entity.getClass().getSimpleName()+" where id= :classId";
        sessionFactory.getCurrentSession().createQuery(deleteById).setLong("classId", id).executeUpdate();
    }

    public  T findById(Serializable id) {
        return (T)sessionFactory.getCurrentSession().get(entity.getClass(), id);
    }

    public  T loadById(Serializable id){
        return (T)sessionFactory.getCurrentSession().load(entity.getClass(), id);
    }

    @Override
    public T createEntity() {
        return entity;
    }

    @Override
    public List list() {
        List list = getCriteria().list();
        return list;
    }

    @Override
    public  List<T> list(int offset, int limit) {
        Criteria listBean = getCriteria();
        listBean.setFirstResult(offset);
        listBean.setMaxResults(limit);
        return listBean.list();
    }

    @Override
    public  int count(){
        return Integer.parseInt(getCriteria().setProjection(Projections.rowCount()).uniqueResult()+"");
    }

}
