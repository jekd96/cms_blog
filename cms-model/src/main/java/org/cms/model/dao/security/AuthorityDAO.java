package org.cms.model.dao.security;

import org.cms.model.dao.CommonDAO;
import org.cms.model.entity.security.Authority;

/**
 * Created by freeman on 23.06.2014.
 */
public interface AuthorityDAO extends CommonDAO<Authority> {
}
