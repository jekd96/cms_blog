package org.cms.model.dao.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.cms.model.entity.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class UserDAOImpl extends SecurityDAOImpl<User> implements UserDAO {
    @Autowired
    public UserDAOImpl(User entity){
        super(entity);
    }

    @Override
    public List<User> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return super.paginate(offset, limit, filterConfig, sortInfo);
    }

}
