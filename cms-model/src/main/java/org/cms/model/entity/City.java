package org.cms.model.entity;

import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * Created by Литвинов on 17.06.2014.
 */
@Entity
@Table(name = "cities")
@Component
public class City extends CommonEntity {
    private Long id;
    private String nameCity = "";
    private Country country;

    @Column(name = "name_city")
    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(String nameCity) {
        this.nameCity = nameCity;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "country_id")
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
