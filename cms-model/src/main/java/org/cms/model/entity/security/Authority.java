package org.cms.model.entity.security;

import org.cms.model.entity.CommonEntity;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "authorities")
@Component
public class Authority extends CommonEntity {

    private String username = "";
    private User user = null;
    private String authority = "";

    @javax.persistence.Column(name = "username")
    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "authority")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @javax.persistence.Column(name = "authority")
    @Basic
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
