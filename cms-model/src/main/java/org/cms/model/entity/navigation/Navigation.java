package org.cms.model.entity.navigation;

import java.util.ArrayList;
import java.util.List;

public class Navigation {

    private Long id = 0L;
    private List<Item> items = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
