package org.cms.model.entity;

import org.springframework.stereotype.Component;

import javax.persistence.Transient;
import java.io.Serializable;

@Component
public class CommonEntity implements Serializable {

    @Transient
    private Long id;
    @Transient
    private Integer version = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Transient
    public String getClassName(){
        return this.getClass().getName();
    }

    @Transient
    public String getClassTitle(){
        return "";
    }

}
