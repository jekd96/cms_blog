
CREATE TABLE users
(
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  enabled  BIT DEFAULT 1,
  name VARCHAR(50),
  family VARCHAR (50),
  image VARCHAR (255),
  country_id BIGINT NOT NULL
);

CREATE TABLE authorities
(
  username  VARCHAR(50) NOT NULL,
  authority VARCHAR(50)
);

CREATE TABLE cities
(
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  name_city VARCHAR(100) NOT NULL,
  country_id BIGINT NOT NULL
);

CREATE TABLE comments
(
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  date DATE,
  content TEXT,
  user_id BIGINT NOT NULL,
  post_id BIGINT NOT NULL
);

CREATE TABLE countries
(
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  name_country VARCHAR (100) NOT NULL
);

CREATE TABLE posts
(
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  user_id BIGINT NOT NULL,
  content TEXT,
  date DATE
);
