CREATE TABLE users
(
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  enabled  BIT DEFAULT 1
);

CREATE TABLE authorities
(
  username  VARCHAR(50) NOT NULL,
  authority VARCHAR(50)
);