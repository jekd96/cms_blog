package org.cms.service;

import org.cms.model.dao.CountryDAO;
import org.cms.model.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Литвинов on 28.06.2014.
 */
@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CountryServiceImpl extends CommonServiceImpl<Country> implements CountryService {
    @Autowired
    public CountryServiceImpl(CountryDAO countryDao){
        setDao(countryDao);
    }
}
