package org.cms.service;

import org.cms.model.dao.CityDAO;
import org.cms.model.entity.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Литвинов on 28.06.2014.
 */
@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CityServiceImpl extends CommonServiceImpl<City> implements CityService  {
    @Autowired
    public CityServiceImpl(CityDAO cityDao){
        setDao(cityDao);
    }
}
