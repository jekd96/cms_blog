package org.cms.service;

import org.cms.model.entity.Comment;

/**
 * Created by Литвинов on 28.06.2014.
 */
public interface CommentService extends CommonService<Comment> {
}
