package org.cms.service;

import org.cms.model.dao.CommonDAO;
import org.cms.model.entity.CommonEntity;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CommonServiceImpl<E extends CommonEntity> implements CommonService<E> {

    protected CommonDAO<E> dao;

    public void setDao(CommonDAO<E> dao) {
        this.dao = dao;
    }

    @Override
    public E add(E bean) {
        return dao.add(bean);
    }

    @Override
    public Criteria getCriteria() {
        return dao.getCriteria();
    }

    @Override
    public E update(E bean) {
        return dao.update(bean);
    }

    @Override
    public void delete(E bean) {
        dao.delete(bean);
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    public E findById(Serializable id) {
        return dao.findById(id);
    }

    @Override
    public E loadById(Serializable id) {
        return dao.loadById(id);
    }

    @Override
    public E createEntity() {
        return dao.createEntity();
    }

    @Override
    public List<E> list() {
        return dao.list();
    }

    @Override
    public List<E> list(int offset, int limit) {
        return dao.list(offset,limit);
    }

    @Override
    public int count() {
        return dao.count();
    }
}
