package org.cms.service;

import org.cms.model.entity.Country;

/**
 * Created by Литвинов on 28.06.2014.
 */
public interface CountryService extends CommonService<Country> {
}
