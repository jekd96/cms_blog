package org.cms.service.security;


import org.cms.model.dao.security.SecurityDAO;
import org.cms.model.entity.CommonEntity;
import org.cms.service.security.acl.AclSecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.model.Sid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public abstract class SecurityServiceImpl<E extends CommonEntity> implements SecurityService<E> {

    SecurityDAO<E> dao;

    @Autowired
    private AclSecurityUtil aclSecurityUtil;

    public void setPermissions(E entity) {
        Sid sidAdmin = new GrantedAuthoritySid("ROLE_ADMIN");
        aclSecurityUtil.addPermission(entity, BasePermission.ADMINISTRATION, entity.getClass());
        aclSecurityUtil.addPermission(entity, sidAdmin, BasePermission.ADMINISTRATION, entity.getClass());
    }


    @Override
    public E create() {
        return dao.createEntity();
    }

    @Override
    public void setDao(SecurityDAO<E> dao) {
        this.dao = dao;
    }

    @Override
    public List<E> list() {
        return dao.list();
    }

    @Override
    public List<E> list(int offset, int limit) {
        return dao.list(offset, limit);
    }

    @Override
    public E update(E entity) {
//        setPermissions(entity);
        return dao.update(entity);
    }

    @Override
    public E add(E entity) {
        return dao.add(entity);
    }

    @Override
    public E find(String value) {
        return null;
    }

    @Override
    public E find(Long value) {
        return null;
    }

    @Override
    public void delete(E entity) {
        dao.delete(entity);
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

}
