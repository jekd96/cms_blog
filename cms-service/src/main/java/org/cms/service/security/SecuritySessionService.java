package org.cms.service.security;


public interface SecuritySessionService {

    public String getUsername();

    
    public String getUserRole();


}
