package org.cms.service.security;


import org.cms.model.dao.security.SecurityDAO;
import org.cms.model.entity.CommonEntity;

import java.util.List;


public interface SecurityService<E extends CommonEntity>{
    void setDao(SecurityDAO<E> dao);
    List<E> list();
    List<E> list(int offset, int limit);
    E update(E entity);
    E add(E entity);
    E create();
    E find(String value);
    E find(Long value);
    void delete(E entity);
    void delete(Long id);
}

