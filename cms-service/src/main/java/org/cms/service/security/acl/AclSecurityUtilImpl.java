package org.cms.service.security.acl;

import org.cms.model.entity.CommonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AclSecurityUtilImpl implements AclSecurityUtil{


    @Autowired
    private MutableAclService aclService;

    @Override
    public void addPermissions(List<? extends CommonEntity> securedList, Permission permission, Class clazz) {
        for(CommonEntity securedEntity : securedList){
            addPermission(securedEntity, permission, clazz);
        }
    }

    @Override
    public void addPermission(CommonEntity securedObject, Permission permission, Class clazz) {
        addPermission(securedObject, new PrincipalSid(getUsername()), permission, clazz);
    }

    @Override
    public void addPermission(CommonEntity securedObject, Sid recipient, Permission permission, Class clazz) {
        MutableAcl acl;
        ObjectIdentity oid = new ObjectIdentityImpl(clazz.getCanonicalName(), securedObject.getId());

        try {
            acl = (MutableAcl) aclService.readAclById(oid);
        } catch (NotFoundException nfe) {
            acl = aclService.createAcl(oid);
        }

        acl.insertAce(acl.getEntries().size(), permission, recipient, true);
        aclService.updateAcl(acl);
    }

    @Override
    public void deletePermission(CommonEntity securedObject, Sid recipient, Permission permission, Class clazz) {
        ObjectIdentity oid = new ObjectIdentityImpl(clazz.getCanonicalName(), securedObject.getId());
        MutableAcl acl = (MutableAcl) aclService.readAclById(oid);

        // Remove all permissions associated with this particular recipient (string equality used to keep things simple)
        List<AccessControlEntry> entries = acl.getEntries();

        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).getSid().equals(recipient) && entries.get(i).getPermission().equals(permission)) {
                acl.deleteAce(i);
            }
        }

        aclService.updateAcl(acl);
    }

    protected String getUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) auth.getPrincipal()).getUsername();
        } else {
            return auth.getPrincipal().toString();
        }
    }

    public MutableAclService getAclService() {
        return aclService;
    }

    public void setAclService(MutableAclService aclService) {
        this.aclService = aclService;
    }
}
