package org.cms.service.security;

import org.cms.model.dao.security.AuthorityDAO;
import org.cms.model.entity.security.Authority;
import org.cms.service.CommonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class AuthorityServiceImpl extends CommonServiceImpl<Authority> implements AuthorityService {

    @Autowired
    public AuthorityServiceImpl(AuthorityDAO dao){
        setDao(dao);
    }

}
