package org.cms.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.cms.model.entity.security.User;
import org.cms.service.Paging;

import java.util.List;

public interface UserService extends SecurityService<User> {

    public Paging.ForUser list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

    public String getCurrentUser();

    public boolean isLive();

}
