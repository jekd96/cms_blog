package org.cms.service.security;

import org.cms.model.entity.security.Authority;
import org.cms.service.CommonService;

public interface AuthorityService extends CommonService<Authority> {
}
