package org.cms.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.cms.model.dao.security.UserDAO;
import org.cms.model.entity.security.User;
import org.cms.service.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class UserServiceImpl extends SecurityServiceImpl<User> implements UserService {

    private UserDAO userDAO;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
        setDao(userDAO);
    }

    public Paging.ForUser list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForUser(userDAO.paginate(offset, limit, filterConfig, sortInfo), userDAO.count(), offset);
    }

    public String getCurrentUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return "contextnulluser";
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public boolean isLive(){
        return true;
    }

}
