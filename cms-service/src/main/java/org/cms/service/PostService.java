package org.cms.service;

import org.cms.model.entity.Post;
import org.cms.service.CommonService;

/**
 * Created by Литвинов on 28.06.2014.
 */
public interface PostService extends CommonService<Post>{
}
