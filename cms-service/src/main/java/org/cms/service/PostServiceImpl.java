package org.cms.service;

import org.cms.model.dao.PostDAO;
import org.cms.model.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Литвинов on 28.06.2014.
 */
@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class PostServiceImpl extends CommonServiceImpl<Post> implements PostService {
    @Autowired
    public void setPostDAO(PostDAO postDAO){
        setDao(postDAO);
    }
}
