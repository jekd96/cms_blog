package org.cms.service;

import org.cms.model.dao.CommentDAO;
import org.cms.model.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Литвинов on 28.06.2014.
 */
@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CommentServiceImpl extends CommonServiceImpl<Comment> implements CommentService{
    @Autowired
    public CommentServiceImpl(CommentDAO commentDao){
        setDao(commentDao);
    }
}
