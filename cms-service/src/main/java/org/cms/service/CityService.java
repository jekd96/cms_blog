package org.cms.service;

import org.cms.model.entity.City;

/**
 * Created by Литвинов on 28.06.2014.
 */
public interface CityService extends CommonService<City>{
}
