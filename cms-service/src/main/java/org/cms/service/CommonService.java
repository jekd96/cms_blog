package org.cms.service;

import org.hibernate.Criteria;

import java.io.Serializable;
import java.util.List;

public interface CommonService<E> {

    E add(E bean);

    Criteria getCriteria();

    E update(E bean);

    void delete(E bean);

    void delete(Long id);

    E findById(Serializable id);

    E loadById(Serializable id);


    E createEntity();

    List<E> list();

    List<E> list(int offset, int limit);

    int count();

}
