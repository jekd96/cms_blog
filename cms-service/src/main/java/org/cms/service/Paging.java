package org.cms.service;

import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.cms.model.entity.security.Authority;
import org.cms.model.entity.security.User;

import java.util.List;

public class Paging {

    public static class ForAuthority extends PagingLoadResultBean<Authority> {
        protected ForAuthority() {}
        public ForAuthority(List<Authority> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForUser extends PagingLoadResultBean<User> {
        protected ForUser() {}
        public ForUser(List<User> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }


}
