import org.cms.model.entity.security.User;
import org.cms.service.security.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:spring-service.xml","classpath:spring-security.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void someTest(){
        //Проверка сервиса пользователей
        for(User user : userService.list()){
            System.out.println(user.getUsername());
        }
    }

}
