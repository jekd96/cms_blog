package org.cms.view.gwt.admin.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.mvp4g.client.view.ReverseViewInterface;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.Viewport;
import org.cms.view.gwt.admin.client.presenter.RootPresenter;

public class Root extends Composite implements ReverseViewInterface<RootPresenter>, RootPresenter.RootInterface {


    private RootPresenter presenter;
    final int LEFT_PANEL_SIZE = 110;
//    final String NOTIFICATION_TITLE = "Сообщения";
    Viewport viewport = new Viewport();
    final private BorderLayoutContainer rootContainer = new BorderLayoutContainer();
    private ContentPanel west = new ContentPanel();
    private ContentPanel east = new ContentPanel();
    private ContentPanel center = new ContentPanel();
    private BorderLayoutData westData = new BorderLayoutData(LEFT_PANEL_SIZE);
    private BorderLayoutData eastData = new BorderLayoutData(LEFT_PANEL_SIZE*5);
    private BorderLayoutData centerData = new BorderLayoutData();
    private Anchor logout = new Anchor("Выход");
 //   private Anchor notifications = new Anchor(NOTIFICATION_TITLE);

    @Override
    public void setPresenter(RootPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public RootPresenter getPresenter() {
        return presenter;
    }

    public void init(){
        GWT.setUncaughtExceptionHandler(new CustomUncaughtExceptionHandler());
        initWidget(viewport);
        viewport.setWidget(rootContainer);
        west.setBodyBorder(true);
        west.setBorders(false);
        center.setBodyBorder(true);
        center.setBorders(false);
        center.setHeaderVisible(true);
     //   center.getHeader().addTool(notifications);
        center.getHeader().addTool(logout);
        logout.setHref(GWT.getHostPageBaseURL() + "logout");
        logout.getElement().setAttribute("style", "padding-right: 15px; color: rgb(0,0,255)");
   //     notifications.getElement().setAttribute("style", "padding-right: 20px; color: rgb(0,0,255)");
        westData.setCollapsible(true);
        westData.setSplit(true);
        westData.setCollapseMini(true);
        eastData.setCollapsible(true);
        eastData.setSplit(true);
        eastData.setCollapseMini(true);
        BorderLayoutData bd = new BorderLayoutData();
        bd.setMargins(new Margins(10));
        rootContainer.setLayoutData(bd);
        rootContainer.setWestWidget(west, westData);
        rootContainer.setCenterWidget(center, centerData);
    }

    public void setNavigatePanel(IsWidget navigatePanel){
        west.setWidget(navigatePanel);
        west.forceLayout();
    }

    public void setDetailsPanel(IsWidget navigatePanel){
        east.setWidget(navigatePanel);
        east.forceLayout();
    }

    public void setContentPanel(IsWidget contentPanel){
        contentPanel.asWidget().setHeight(""+center.getOffsetHeight());
        center.setWidget(contentPanel);
    }

}