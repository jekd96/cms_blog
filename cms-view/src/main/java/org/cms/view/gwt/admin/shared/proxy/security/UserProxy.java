package org.cms.view.gwt.admin.shared.proxy.security;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.security.User;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


@ProxyFor(value = User.class)
public interface UserProxy extends CommonProxy {

    public String getUsername();
    public void setUsername(String username);

    public String getPassword();
    public void setPassword(String username);

    public Boolean getEnabled();
    public void setEnabled(Boolean enabled);

    public AuthorityProxy getAuthority();
    public void setAuthority(AuthorityProxy authority);

}

