package org.cms.view.gwt.admin.client.view.widget.editor.security;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.sencha.gxt.widget.core.client.form.TextField;
import org.cms.view.gwt.admin.client.view.widget.editor.AbstractEditor;
import org.cms.view.gwt.admin.shared.proxy.security.UserProxy;
import org.cms.view.gwt.admin.shared.request.security.UserRequestFactory;

public class UserEditor extends AbstractEditor<UserProxy, UserEditor, UserRequestFactory> {
    public interface UserDriver extends RequestFactoryEditorDriver<UserProxy, UserEditor> {}


    @Editor.Path("username")
    TextField username = new TextField();
    @Editor.Path("enabled")
    CheckBox enabled = new CheckBox();

    public UserEditor() {
        initEditor();
    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(UserRequestFactory.class);
        driver = GWT.create(UserDriver.class);
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(username, "Username"));
        addField(createFieldLabel(enabled, "Enabled"));
    }

    @Override
    protected void receiveData(){
    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }

    @Override
    public void clear() {

    }
}