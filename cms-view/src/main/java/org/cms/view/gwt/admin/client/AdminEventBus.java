package org.cms.view.gwt.admin.client;


import com.google.gwt.user.client.ui.IsWidget;
import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.annotation.Filters;
import com.mvp4g.client.annotation.Start;
import com.mvp4g.client.event.EventBus;
import org.cms.view.gwt.admin.client.presenter.ContentPanelPresenter;
import org.cms.view.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.cms.view.gwt.admin.client.presenter.RootPresenter;
import org.cms.view.gwt.admin.client.presenter.SecurityFilter;


/**
 * @author almagnit@gmail.com
 *
 */

@Filters(filterClasses = SecurityFilter.class)
@Events( startPresenter = RootPresenter.class)
public interface AdminEventBus extends EventBus {

    @Start
    @Event(handlers = {RootPresenter.class, NavigatePanelPresenter.class, ContentPanelPresenter.class})
    public void start();

    @Event(handlers = RootPresenter.class)
    public void changeNavigatePanel(IsWidget navigatePanel);

    @Event(handlers = RootPresenter.class)
    public void changeDetailsPanel(IsWidget datailsPanel);

    @Event(handlers = RootPresenter.class)
    public void changeContentPanel(IsWidget contentPanel);

    @Event(handlers = ContentPanelPresenter.class, navigationEvent = true)
    public void loadUsers();



}
