package org.cms.view.gwt.admin.shared.proxy;

        import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.City;

/**
 * Created by Литвинов on 29.06.2014.
 */
@ProxyFor(value=City.class)
public interface CityProxy extends CommonProxy{
    public String getNameCity();
    public void setNameCity(String nameCity);
    public Long getId();
    public void setId(Long id);
  //  public Country getCountry();
 //   public void setCountry(Country country);

}
