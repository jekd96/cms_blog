package org.cms.view.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

public interface CommonRequestFactory extends RequestFactory {

    public CommonRequestContext context();

}
