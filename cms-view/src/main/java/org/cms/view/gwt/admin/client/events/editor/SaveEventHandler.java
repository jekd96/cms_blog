package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public interface SaveEventHandler<T extends CommonProxy> extends EventHandler {

    void onSave(SaveEvent<T> event);

}
