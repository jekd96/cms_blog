package org.cms.view.gwt.admin.client.view.container.admin;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer.AccordionLayoutAppearance;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import org.cms.view.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.cms.view.gwt.admin.client.view.container.NavigatePanel;


public class NavigateAdminPanel extends VBoxLayoutContainer implements NavigatePanel {

    private final String BUTTON_HEIGHT = "30";

    private NavigatePanelPresenter presenter;

    private ContentPanel contentPanel = new ContentPanel();
    private BoxLayoutData contentPanelLayoutData = new BoxLayoutData();
    private AccordionLayoutContainer accordionContainer = new AccordionLayoutContainer();
    private AccordionLayoutAppearance accordionAppearance = GWT.<AccordionLayoutAppearance> create(AccordionLayoutAppearance.class);
    private ContentPanel hrPanel = new ContentPanel(accordionAppearance);
    private VBoxLayoutContainer hrContainer = new VBoxLayoutContainer();

    private ToggleGroup buttons;
    private ToggleButton users;

    public NavigateAdminPanel() {
        this.setBorders(false);
        buttons = new ToggleGroup();
        contentPanelLayoutData.setFlex(1);
        this.add(contentPanel, contentPanelLayoutData);
        contentPanel.add(accordionContainer, contentPanelLayoutData);
        accordionContainer.add(hrPanel);
        accordionContainer.setBorders(false);
        hrPanel.setHeadingText("Разделы");
        hrPanel.add(hrContainer);
        contentPanel.setResize(true);
        contentPanel.setHeaderVisible(false);
        contentPanel.setBorders(false);
        contentPanel.setBodyBorder(false);
        hrPanel.setBorders(false);
        hrPanel.setBodyBorder(false);
        hrContainer.setBorders(false);
        hrContainer.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
        this.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
    }

    public void setPresenter(NavigatePanelPresenter presenter) {
        this.presenter = presenter;
    }

    public NavigatePanelPresenter getPresenter() {
        return presenter;
    }

    public void init(){
        initButtons();
        initHandlers();
    }

    private void initButtons(){
        users = new ToggleButton("Пользователи");
        initHrButton(users);
    }

    private void initHrButton(ToggleButton button){
        initButton(button);
        hrContainer.add(button);
    }

    private void initButton(ToggleButton button){
        button.setHeight(BUTTON_HEIGHT);
        button.setAllowDepress(false);
        buttons.add(button);
    }

    private void initHandlers(){

        users.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    presenter.getEventBus().loadUsers();
                }
            }
        });
    }

}
