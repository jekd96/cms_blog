package org.cms.view.gwt.admin.server;

import com.google.web.bindery.requestfactory.server.ServiceLayerDecorator;
import com.google.web.bindery.requestfactory.shared.Locator;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceLocator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;


@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@Service("gwtSpringServiceLayerDecorator")
public class GWTSpringServiceLayerDecorator extends ServiceLayerDecorator implements ApplicationContextAware {

    @Autowired
    ApplicationContext context;
    @Override
    public <T extends Locator<?, ?>> T createLocator(Class<T> clazz) {
        return context.getBean(clazz);
    }

    @Override
    public Object createServiceInstance(Class<? extends RequestContext> requestContext) {
        Class<? extends ServiceLocator> locatorType = getTop().resolveServiceLocator(requestContext);
        assert locatorType != null;
        ServiceLocator locator = context.getBean(locatorType);
        Class<?> serviceClass = getTop().resolveServiceClass(requestContext);
        return locator.getInstance(serviceClass);
    }

    @Override
    public Object invoke(Method domainMethod, Object... args) {
        Object response = super.invoke(domainMethod, args);
        try {
            if (response instanceof Callable) {
                return ((Callable<?>) response).call();
            } else {
                return response;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }
}