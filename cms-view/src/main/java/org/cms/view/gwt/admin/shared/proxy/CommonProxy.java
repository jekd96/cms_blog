package org.cms.view.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.BaseProxy;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import org.cms.model.entity.CommonEntity;



@ProxyFor(CommonEntity.class)
public interface CommonProxy extends BaseProxy, EntityProxy, ValueProxy {

    public Long getId();

    public void setId(Long id);

    public Integer getVersion();

    public void setVersion(Integer version);

}