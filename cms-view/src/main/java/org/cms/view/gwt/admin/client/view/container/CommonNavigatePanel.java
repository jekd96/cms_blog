package org.cms.view.gwt.admin.client.view.container;


import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.view.ReverseViewInterface;
import org.cms.view.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.cms.view.gwt.admin.client.view.container.admin.NavigateAdminPanel;


public class CommonNavigatePanel implements ReverseViewInterface<NavigatePanelPresenter>, NavigatePanelPresenter.NavigatePanelInterface {

    private NavigatePanelPresenter presenter;
    private NavigatePanel navigatePanel;

    @Override
    public void setPresenter(NavigatePanelPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public NavigatePanelPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void init(String role) {
        navigatePanel = new NavigateAdminPanel();
        navigatePanel.setPresenter(presenter);
        navigatePanel.init();
    }

    @Override
    public Widget asWidget() {
        return navigatePanel.asWidget();
    }
}
