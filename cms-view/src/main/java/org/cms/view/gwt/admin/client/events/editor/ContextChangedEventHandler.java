package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;



public interface ContextChangedEventHandler extends EventHandler {

    void beforeContextChanged(ContextChangedEvent event);

}
