package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;


public interface BeforeSaveEventHandler extends EventHandler {

    void beforeSave(BeforeSaveEvent event);

}
