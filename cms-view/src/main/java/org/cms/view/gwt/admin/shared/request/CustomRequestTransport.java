package org.cms.view.gwt.admin.shared.request;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import com.google.web.bindery.requestfactory.gwt.client.DefaultRequestTransport;
import com.google.web.bindery.requestfactory.shared.ServerFailure;


public class CustomRequestTransport extends DefaultRequestTransport {

    @Override
    public void send( String payload, TransportReceiver receiver ) {
        doBeforeSend();
        super.send( payload, wrap( receiver ) );
    }

    private TransportReceiver wrap( final TransportReceiver delegate ) {
        return new TransportReceiver() {

            public void onTransportSuccess( String payload ) {
                doOnSuccess();
                delegate.onTransportSuccess( payload );
            }

            public void onTransportFailure( ServerFailure failure ) {
                doOnFailure( failure );
                delegate.onTransportFailure( failure );
            }
        };
    }

    protected void doBeforeSend() {
        // Some processing before the request is send
        Document.get().getBody().getStyle().setCursor( Style.Cursor.WAIT );
    }

    protected void doOnSuccess() {
        // Some processing on success
        Document.get().getBody().getStyle().setCursor( Style.Cursor.DEFAULT );
    }

    protected void doOnFailure( ServerFailure failure ) {
        // Some processing on failure
        Document.get().getBody().getStyle().setCursor( Style.Cursor.DEFAULT );
    }
}