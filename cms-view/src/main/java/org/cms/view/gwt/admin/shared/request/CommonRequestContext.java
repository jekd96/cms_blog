package org.cms.view.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.service.CommonService;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;

import java.util.List;


@Service(value = CommonService.class)
public interface CommonRequestContext<P extends CommonProxy> extends RequestContext {
    Request<List<P>> list();
    Request<P> update(P entity);
    Request<P> add(P entity);
 //   Request<P> createEntity();
 //   Request<P> findById(Long id);
    Request<Void> delete(Long id);
    Request<Void> delete(P entity);

}