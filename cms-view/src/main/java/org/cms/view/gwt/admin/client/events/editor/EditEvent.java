package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public class EditEvent<T extends CommonProxy> extends GwtEvent<EditEventHandler>{

    public static Type<EditEventHandler> TYPE = new Type<EditEventHandler>();

    T entity;

    public EditEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<EditEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditEventHandler handler) {
        handler.onEdit(this);
    }

    public T getEntity() {
        return entity;
    }
}