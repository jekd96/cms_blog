package org.cms.view.gwt.admin.shared.proxy.security;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.security.Authority;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


@ProxyFor(Authority.class)
public interface AuthorityProxy extends CommonProxy {

    public String getUsername();
    public void setUsername(String username);

    public String getAuthority();
    public void setAuthority(String authority);

    public UserProxy getUser();
    public void setUser(UserProxy user);

}