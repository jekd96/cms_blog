package org.cms.view.gwt.client.client;

import com.mvp4g.client.annotation.Event;
import com.mvp4g.client.annotation.Events;
import com.mvp4g.client.annotation.Start;
import com.mvp4g.client.event.EventBus;
import org.cms.view.gwt.client.client.presenter.RootPresenter;

@Events( startPresenter = RootPresenter.class)
public interface ClientEventBus extends EventBus {

    @Start
    @Event(handlers = {RootPresenter.class})
    public void start();

}