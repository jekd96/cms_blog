package org.cms.view.gwt.admin.shared.request.security;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.service.security.SecuritySessionService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;


public interface SecuritySessionRequestFactory extends RequestFactory {

    @Service(value = SecuritySessionService.class, locator = GWTSpringServiceLocator.class)
    public interface SecuritySessionRequest extends RequestContext {
        Request<String> getUsername();
        Request<String> getUserRole();
    }

    SecuritySessionRequest context();

}