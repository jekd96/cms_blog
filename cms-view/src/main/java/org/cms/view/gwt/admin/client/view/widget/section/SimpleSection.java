package org.cms.view.gwt.admin.client.view.widget.section;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.AbstractEventCell;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.MoveEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;
import org.cms.view.gwt.admin.client.events.editor.*;
import org.cms.view.gwt.admin.client.view.widget.component.DeleteMessageBox;
import org.cms.view.gwt.admin.client.view.widget.editor.AbstractEditor;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;

import java.util.List;


public class SimpleSection<T extends CommonProxy, E extends AbstractEditor> extends AbstractSection<T, E> implements IsWidget {

    protected final int TOOLBAR_HEIGHT = 30;

    protected VerticalLayoutContainer editorContainer = new VerticalLayoutContainer();
    @Editor.Ignore
    protected Grid<T> contentGrid = null;
    protected LiveGridView<T> liveGridView;
    protected List<ColumnConfig<T, ?>> columns;
    @Editor.Ignore
    protected Window editorWindow = new Window();
    @Editor.Ignore
    protected Window searchWindow = new Window();
    @Editor.Ignore
    protected TextButton save = new TextButton("Соханить");
    @Editor.Ignore
    protected TextButton cancel = new TextButton("Отмена");
    @Editor.Ignore
    protected TextButton entityExport = new TextButton("Выгрузить");
    @Editor.Ignore
    protected TextButton referenceExport = new TextButton("Выгрузить");
    @Editor.Ignore
    protected TextButton create = new TextButton("Создать");
    @Editor.Ignore
    protected TextButton edit = new TextButton("Редактировать");
    @Editor.Ignore
    protected TextButton delete = new TextButton("Удалить");
    @Editor.Ignore
    protected TextButton refresh = new TextButton("Обновить");
    @Editor.Ignore
    protected TextButton search = new TextButton("Поиск");

    private HorizontalLayoutContainer commandPanel = new HorizontalLayoutContainer();
    private ToolBar commandToolBar = new ToolBar();
    private Menu contextMenu = new Menu();
    private MenuItem copy = new MenuItem();

    public SimpleSection(E editor) {
        setEditor(editor);
        commandToolBar.setHeight(TOOLBAR_HEIGHT);
        commandToolBar.add(create);
        commandToolBar.add(edit);
        commandToolBar.add(delete);
        commandToolBar.add(refresh);
        commandToolBar.add(search);
        commandToolBar.add(referenceExport);
        commandToolBar.setLayoutData(new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        commandPanel.add(commandToolBar);
        editorContainer.add(commandPanel, new VerticalLayoutContainer.VerticalLayoutData(1, TOOLBAR_HEIGHT));
        editorWindow.setBodyBorder(false);
        editorWindow.setModal(true);
        editorWindow.addButton(save);
        editorWindow.addButton(cancel);
        editorWindow.addButton(entityExport);
        liveGridView = new LiveGridView<T>(){
            @Override
            public void refresh(boolean headerToo) {
                preventScrollToTopOnRefresh = true;
                super.refresh(headerToo);
            }
        };
        liveGridView.setPrefetchFactor(.6);
        liveGridView.setCacheSize(300);
        liveGridView.setForceFit(false);
        initReceivers();

        searchWindow.setBodyBorder(false);
        searchWindow.setModal(true);
        editorWindow.addButton(cancel);
        initContextMenu();
    }

    private void initContextMenu(){
        copy.setText("Копировать");
        copy.getElement().setId("context_copy");
        contextMenu.add(copy);
        contextMenu.addAttachHandler(new AttachEvent.Handler() {
            @Override
            public void onAttachOrDetach(AttachEvent event) {
                doCopy();
            }
        });
    }

    public void addCommandTool(Widget child) {
        commandToolBar.add(child);
    }

    public void setToolBar(ToolBar toolBar){
        commandPanel.clear();
        commandPanel.add(toolBar, new HorizontalLayoutContainer.HorizontalLayoutData(1,1));
    }

    public ToolBar getToolBar() {
        return commandToolBar;
    }

    public void initReceivers(){
        editor.addSaveEventHandler(new SaveEventHandler() {
            @Override
            public void onSave(SaveEvent event) {
                hideEditor();
                refreshEditedEntity((T)event.getEntity());
            }
        });
        editor.addUpdateEventHandler(new UpdateEventHandler() {
            @Override
            public void onUpdate(UpdateEvent event) {
                refreshEditedEntity((T) event.getEntity());
            }
        });
        editor.addDeleteEventHandler(new DeleteEventHandler() {
            @Override
            public void onDelete(DeleteEvent event) {
                contentGrid.getStore().remove((T) event.getEntity());
                contentGrid.getView().refresh(false);
            }
        });
        save.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                save();
                refreshGrid();
            }
        });

        cancel.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                hideEditor();
            }
        });
        entityExport.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
//                exportEntityData();
            }
        });
        referenceExport.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
//                exportReferenceData();
            }
        });
        create.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                create();
                showEditor();
            }
        });
        edit.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                edit(contentGrid.getSelectionModel().getSelectedItem());
                showEditor();
            }
        });
        delete.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                deleteSelected(contentGrid.getSelectionModel().getSelectedItems());
            }
        });
        refresh.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                refreshGrid();
            }
        });
        search.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                searchEditor();
            }
        });
        editorWindow.addHideHandler(new HideEvent.HideHandler() {
            @Override
            public void onHide(HideEvent event) {
                clearEditor();
            }
        });
    }

    private void deleteSelected(final List<T> beans) {
        final DeleteMessageBox messageBox = new DeleteMessageBox("Удаление", "Удалить выделенные объекты?", true);
        messageBox.addOnDeleteHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                for(T bean : beans) {
                    delete(bean);
                }
            }
        });
    }

    public void refreshEditedEntity(T entity) {
        if(entity == null)return;
        if(contentGrid.getStore().getAll().contains(entity)){
            contentGrid.getStore().update(entity);
        }else{
            contentGrid.getStore().add(entity);
        }
//        contentGrid.getView().refresh(false);
    }


    @Override
    public Widget asWidget() {
        return editorContainer;
    }

    public void setToolBarVisible(boolean visible){
        commandPanel.setVisible(visible);
    }

    private static native void initCopy()/*-{
        var clip = new $wnd.ZeroClipboard($doc.getElementById("context_copy"));
        clip.on( 'noflash', $entry(function ( client, args ) {
            alert("You don't support flash");
        }));
        clip.on( 'wrongflash', $entry(function ( client, args ) {
            alert("Your flash is too old " + args.flashVersion);
        }));
    }-*/;

    private static native void refreshCopy(String text)/*-{
        var clip = new $wnd.ZeroClipboard($doc.getElementById("context_copy"));
        clip.setText(text);
    }-*/;

    private void doCopy(){
        List<T> selectedItems = contentGrid.getSelectionModel().getSelectedItems();
        String toCopy = "";
        for (T item : selectedItems){
            toCopy +=  item.getId()+"\n";
        }
        refreshCopy(toCopy);
    }

    public Grid<T> getContentGrid() {
        return contentGrid;
    }

    public void setContentGrid(Grid<T> _contentGrid) {
        this.contentGrid = _contentGrid;
        this.contentGrid.setView(liveGridView);
        editorContainer.add(this.contentGrid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        this.contentGrid.setLoadMask(true);
        create.setEnabled(true);
        contentGrid.setContextMenu(contextMenu);
        contentGrid.addMoveHandler(new MoveEvent.MoveHandler() {
            @Override
            public void onMove(MoveEvent event) {
                initCopy();
            }
        });
        contentGrid.addRowDoubleClickHandler(new RowDoubleClickEvent.RowDoubleClickHandler()
        {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                edit(contentGrid.getSelectionModel().getSelectedItem());
                showEditor();
            }
        });
    }

    public void setContentGrid(Grid<T> _contentGrid, RowDoubleClickEvent.RowDoubleClickHandler rowClickHandler) {
        this.contentGrid = _contentGrid;
        this.contentGrid.setView(liveGridView);
        editorContainer.add(this.contentGrid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        this.contentGrid.setLoadMask(true);
        create.setEnabled(true);
        contentGrid.addRowDoubleClickHandler(rowClickHandler);
    }

    public List<ColumnConfig<T, ?>> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnConfig<T, ?>> columns) {
        this.columns = columns;
    }

    @Override
    public void setEditor(E editor) {
        setEditorPanel(editor.asWidget());
        super.setEditor(editor);
    }

    public void setEditorPanel(Widget fieldsPanel) {
        editorWindow.setWidget(fieldsPanel);
    }

    public void addButton(Widget button) {
        editorWindow.addButton(button);
    }

    public void showEditor(){
        editorWindow.show();
    }

    public void hideEditor(){
        editorWindow.hide();
    }

    public void refreshGrid() {
        liveGridView.refresh();
    }

    public void searchEditor() {
        searchWindow.show();
    }

    public <N extends Boolean> ColumnConfig<T, N> getBooleanColumnConfig(String trueValue, String falseValue, ValueProvider<? super T, N> valueProvider, int width, String header){
        ColumnConfig<T, N> carTypeConfig = new ColumnConfig<T, N>(valueProvider, width, header);
        carTypeConfig.setCell(new BooleanToStringCell<N>(trueValue, falseValue));
        return carTypeConfig;
    }

    private class BooleanToStringCell<N extends Boolean> extends AbstractEventCell<N> {

        String trueValue = null;
        String falseValue = null;

        private BooleanToStringCell(String trueValue, String falseValue) {
            this.trueValue = trueValue;
            this.falseValue = falseValue;
        }

        @Override
        public void render(Context context, N value, SafeHtmlBuilder sb) {
            sb.appendEscaped(Boolean.TRUE.equals(value) ? trueValue : falseValue);
        }

    }
}
