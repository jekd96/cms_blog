package org.cms.view.gwt.admin.client.view.widget.component;

import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;


    public class LocalDateField extends DateField {

    public LocalDateField() {
        super();
        this.setPropertyEditor(new DateTimePropertyEditor("yyyy.MM.dd"));

    }

}
