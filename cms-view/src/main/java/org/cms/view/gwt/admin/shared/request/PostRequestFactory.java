package org.cms.view.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.service.CountryService;
import org.cms.service.PostService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.CountryProxy;
import org.cms.view.gwt.admin.shared.proxy.PostProxy;

import java.util.List;

/**
 * Created by Литвинов on 29.06.2014.
 */
public interface PostRequestFactory extends CommonRequestFactory {
    @Service(value = PostService.class, locator = GWTSpringServiceLocator.class)
    public interface PostRequest extends CommonRequestContext<PostProxy>{
        Request<List<PostProxy>> list();
        Request<PostProxy> update(PostProxy entity);
        Request<PostProxy> add(PostProxy entity);
        Request<PostProxy> createEntity();
        Request<PostProxy> findById(Long id);
        Request<Void> delete(Long id);
        Request<Void> delete(PostProxy entity);
    }

    PostRequest context();

}
