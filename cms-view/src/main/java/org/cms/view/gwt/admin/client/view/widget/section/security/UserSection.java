package org.cms.view.gwt.admin.client.view.widget.section.security;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.*;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import org.cms.view.gwt.admin.client.property.security.UserProperties;
import org.cms.view.gwt.admin.client.view.widget.editor.security.UserEditor;
import org.cms.view.gwt.admin.client.view.widget.section.SimpleSection;
import org.cms.view.gwt.admin.shared.proxy.security.UserProxy;
import org.cms.view.gwt.admin.shared.request.security.UserRequestFactory;

import java.util.ArrayList;
import java.util.List;


public class UserSection implements IsWidget {

    Grid<UserProxy> contentGrid = null;
    UserProperties props = GWT.create(UserProperties.class);
    List<ColumnConfig<UserProxy, ?>> columns = new ArrayList<ColumnConfig<UserProxy, ?>>();
    UserEditor editor = new UserEditor();
    SimpleSection<UserProxy, UserEditor> userSection = new SimpleSection<UserProxy, UserEditor>(editor);

    public UserSection() {
        initGrid();
    }

    public Widget asWidget() {
        return userSection.asWidget();
    }

    public void initGrid(){
        columns.add(new ColumnConfig<UserProxy, String>(props.username(), 100, "Логин"));
        columns.add(new ColumnConfig<UserProxy, Boolean>(props.enabled(), 50, "Блокировка"));
        RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<UserProxy>> proxy =
                new RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<UserProxy>>() {
                    @Override
                    public void load(FilterPagingLoadConfig loadConfig, Receiver<? super PagingLoadResult<UserProxy>> receiver) {
                        UserRequestFactory.UserRequest req = editor.getService().context();

                        List<SortInfo> sortInfo = createRequestSortInfo(req, loadConfig.getSortInfo());
                        List<FilterConfig> filterConfig = createRequestFilterConfig(req, loadConfig.getFilters());
                        req.list(loadConfig.getOffset(), loadConfig.getLimit(), filterConfig, sortInfo).to(receiver);

                        req.fire();
                    }
                };
        final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<UserProxy>> loader =
                new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<UserProxy>>(proxy) {
                    @Override
                    protected FilterPagingLoadConfig newLoadConfig() {
                        return new FilterPagingLoadConfigBean();
                    }
                };



        contentGrid = new Grid<UserProxy>(new ListStore<UserProxy>(props.key()), new ColumnModel<UserProxy>(columns)) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        loader.load(0, ((LiveGridView)contentGrid.getView()).getCacheSize());
                    }
                });
            }
        };
        loader.setRemoteSort(true);
        loader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, UserProxy, PagingLoadResult<UserProxy>>(contentGrid.getStore()));
        GridFilters<UserProxy> filters = new GridFilters<UserProxy>(loader);
        filters.initPlugin(contentGrid);
        contentGrid.setLoader(loader);
        userSection.setColumns(columns);
        userSection.setContentGrid(contentGrid);
    }


}
