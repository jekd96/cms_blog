package org.cms.view.gwt.admin.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;
import org.cms.view.gwt.admin.client.AdminEventBus;
import org.cms.view.gwt.admin.client.view.container.admin.ContentAdminPanel;
import org.cms.view.gwt.admin.client.view.widget.section.security.UserSection;


@Presenter(view = ContentAdminPanel.class)
public class ContentPanelPresenter extends BasePresenter<ContentPanelPresenter.ContentPanelInterface, AdminEventBus>{

    public void onStart(){
        GWT.runAsync(new RunAsyncCallback() {
            @Override
            public void onFailure(Throwable reason) {
                com.google.gwt.user.client.Window.alert("Code download failed: " + reason.getMessage());
            }

            @Override
            public void onSuccess() {
                getEventBus().changeContentPanel(view.asWidget());
            }
        });
    }

    public void onLoadUsers(){
        GWT.runAsync(new RunAsyncCallback() {
            @Override
            public void onFailure(Throwable reason) {
                com.google.gwt.user.client.Window.alert("Code download failed: " + reason.getMessage());
            }

            @Override
            public void onSuccess() {
                view.loadData(new UserSection());
            }
        });
    }

    public interface ContentPanelInterface extends IsWidget {

        public void loadData(IsWidget editor);

    }
}
