package org.cms.view.gwt.admin.client.view.widget.component;

import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.cell.core.client.form.TriggerFieldCell;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ListView;


public class ComboBox<T> extends com.sencha.gxt.widget.core.client.form.ComboBox<T> {

    {
        this.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
    }

    public ComboBox(ComboBoxCell<T> cell) {
        super(cell);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider) {
        super(store, labelProvider);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider, ListView<T, ?> listView) {
        super(store, labelProvider, listView);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider, ListView<T, ?> listView, TriggerFieldCell.TriggerFieldAppearance appearance) {
        super(store, labelProvider, listView, appearance);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider, SafeHtmlRenderer<T> renderer) {
        super(store, labelProvider, renderer);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider, SafeHtmlRenderer<T> renderer, TriggerFieldCell.TriggerFieldAppearance appearance) {
        super(store, labelProvider, renderer, appearance);
    }

    public ComboBox(ListStore<T> store, LabelProvider<? super T> labelProvider, TriggerFieldCell.TriggerFieldAppearance appearance) {
        super(store, labelProvider, appearance);
    }

}
