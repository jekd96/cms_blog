package org.cms.view.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.Country;

@ProxyFor(value = Country.class)
public interface CountryProxy extends CommonProxy {

    public Long getId();
    public void setId(Long id);

    public String getNameCountry();
    public void setNameCountry(String nameCountry);

  //  public List<City> getCities();
  //  public void setCities(List<City> cities);

}
