package org.cms.view.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.service.CommentService;
import org.cms.service.PostService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.CommentProxy;
import org.cms.view.gwt.admin.shared.proxy.CountryProxy;
import org.cms.view.gwt.admin.shared.proxy.PostProxy;

import java.util.List;

/**
 * Created by Литвинов on 29.06.2014.
 */
public interface CommentRequestFactory extends CommonRequestFactory {
    @Service(value = CommentService.class, locator = GWTSpringServiceLocator.class)
    public interface CommentRequest extends CommonRequestContext<CommentProxy>{
        Request<List<CommentProxy>> list();
        Request<CommentProxy> update(CommentProxy entity);
        Request<CommentProxy> add(CommentProxy entity);
        Request<CommentProxy> createEntity();
        Request<CommentProxy> findById(Long id);
        Request<Void> delete(Long id);
        Request<Void> delete(CommentProxy entity);
    }

    CommentRequest context();

}
