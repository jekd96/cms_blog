package org.cms.view.gwt.admin.shared.request;


import com.google.web.bindery.requestfactory.shared.Request;
import org.cms.service.CountryService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.CountryProxy;
import com.google.web.bindery.requestfactory.shared.Service;


import java.util.List;

public interface CountryRequestFactory extends CommonRequestFactory {

    @Service(value = CountryService.class, locator = GWTSpringServiceLocator.class)
    public interface CountryRequest extends CommonRequestContext<CountryProxy>{
        Request<List<CountryProxy>> list();
        Request<CountryProxy> update(CountryProxy entity);
        Request<CountryProxy> add(CountryProxy entity);
        Request<CountryProxy> createEntity();
        Request<CountryProxy> findById(Long id);
        Request<Void> delete(Long id);
        Request<Void> delete(CountryProxy entity);
    }

    CountryRequest context();

}
