package org.cms.view.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.service.CityService;
import org.cms.service.PostService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.CityProxy;
import org.cms.view.gwt.admin.shared.proxy.CountryProxy;
import org.cms.view.gwt.admin.shared.proxy.PostProxy;

import java.util.List;

/**
 * Created by Литвинов on 29.06.2014.
 */
public interface CityRequestFactory extends CommonRequestFactory {
    @Service(value = CityService.class, locator = GWTSpringServiceLocator.class)
    public interface CityRequest extends CommonRequestContext<CityProxy>{
        Request<List<CityProxy>> list();
        Request<CityProxy> update(CityProxy entity);
        Request<CityProxy> add(CityProxy entity);
        Request<CityProxy> createEntity();
        Request<CityProxy> findById(Long id);
        Request<Void> delete(Long id);
        Request<Void> delete(CityProxy entity);
    }

    CityRequest context();

}
