package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;

public class SaveEvent<T extends CommonProxy> extends GwtEvent<SaveEventHandler>{

    public static Type<SaveEventHandler> TYPE = new Type<SaveEventHandler>();

    T entity;

    public SaveEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<SaveEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(SaveEventHandler handler) {
        handler.onSave(this);
    }

    public T getEntity() {
        return entity;
    }
}