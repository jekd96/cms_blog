package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;


public interface BeforeEditEventHandler extends EventHandler {

    void onBeforeEdit(BeforeEditEvent event);

}
