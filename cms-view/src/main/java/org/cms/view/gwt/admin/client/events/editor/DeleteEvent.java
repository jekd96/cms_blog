package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;

public class DeleteEvent<T extends CommonProxy> extends GwtEvent<DeleteEventHandler>{

    public static Type<DeleteEventHandler> TYPE = new Type<DeleteEventHandler>();

    T entity;

    public DeleteEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<DeleteEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(DeleteEventHandler handler) {
        handler.onDelete(this);
    }

    public T getEntity() {
        return entity;
    }
}