package org.cms.view.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.Comment;

/**
 * Created by Литвинов on 29.06.2014.
 */
@ProxyFor(value= Comment.class)
public interface CommentProxy extends CommonProxy{
    public Long getId();
    public void setId(Long id);
    /*
    public Date getDate();
    public void setDate(Date date);
    public String getContent();
    public void setContent(String content);
    public User getUser();
    public void setUser(User user);
    public Post getPost();
    public void setPost(Post post);*/
}
