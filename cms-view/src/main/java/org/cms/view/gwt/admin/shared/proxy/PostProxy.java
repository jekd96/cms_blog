package org.cms.view.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.cms.model.entity.Post;

/**
 * Created by Литвинов on 29.06.2014.
 */
@ProxyFor(value= Post.class)
public interface PostProxy extends CommonProxy {
    public Long getId();
    public void setId(Long id);
    /*
    public User getUser();
    public void setUser(User user);
    public String getContent();
    public void setContent(String content);
    public List<Comment> getComments();
    public void setComments(List<Comment> comments);
    public Date getDate();
    public void setDate(Date date);*/
}
