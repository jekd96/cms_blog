package org.cms.view.gwt.admin.client.property.security;


import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import org.cms.view.gwt.admin.shared.proxy.security.UserProxy;


public interface UserProperties extends PropertyAccess<UserProxy> {

    @Editor.Path("username")
    com.sencha.gxt.data.shared.ModelKeyProvider<UserProxy> key();

    @Editor.Path("username")
    ValueProvider<UserProxy, String> username();
    @Editor.Path("password")
    ValueProvider<UserProxy, String> password();
    @Editor.Path("enabled")
    ValueProvider<UserProxy, Boolean> enabled();

}
