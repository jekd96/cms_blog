package org.cms.view.gwt.admin.server;

import com.google.web.bindery.requestfactory.server.ExceptionHandler;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoquaciousExceptionHandler implements ExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(LoquaciousExceptionHandler.class);

    @Override
    public ServerFailure createServerFailure( Throwable throwable ) {
        LOG.error( "Server error", throwable );
        throwable.printStackTrace();
        return new ServerFailure( throwable.getMessage(), throwable.getClass().getName(), null, true );
    }


}