package org.cms.view.gwt.client.client.presenter;

import com.google.gwt.user.client.ui.IsWidget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;
import org.cms.view.gwt.client.client.ClientEventBus;
import org.cms.view.gwt.client.client.Root;

@Presenter( view = Root.class )
public class RootPresenter extends BasePresenter<RootPresenter.RootInterface, ClientEventBus> {

    public void onStart(){
        view.init();
    }

    public static interface RootInterface extends IsWidget {

        public void init();
    }
}
