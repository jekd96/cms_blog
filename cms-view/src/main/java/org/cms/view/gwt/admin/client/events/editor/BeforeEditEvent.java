package org.cms.view.gwt.admin.client.events.editor;

import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public class BeforeEditEvent<T extends CommonProxy> extends GwtEvent<BeforeEditEventHandler>{

    public static Type<BeforeEditEventHandler> TYPE = new Type<BeforeEditEventHandler>();

    T entity;

    public BeforeEditEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<BeforeEditEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(BeforeEditEventHandler handler) {
        handler.onBeforeEdit(this);
    }

    public T getEntity() {
        return entity;
    }
}