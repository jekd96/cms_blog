package org.cms.view.gwt.admin.shared.request.security;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import org.cms.model.entity.security.Authority;
import org.cms.service.security.AuthorityService;
import org.cms.service.security.UserService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.security.AuthorityProxy;
import org.cms.view.gwt.admin.shared.proxy.security.UserProxy;
import org.cms.view.gwt.admin.shared.request.CommonRequestContext;
import org.cms.view.gwt.admin.shared.request.CommonRequestFactory;

import java.util.List;

public interface AuthorityRequestFactory extends CommonRequestFactory {

    @Service(value = AuthorityService.class, locator = GWTSpringServiceLocator.class)
    public interface AuthorityRequest extends CommonRequestContext<AuthorityProxy> {
        Request<List<AuthorityProxy>> list();
        Request<AuthorityProxy> update(AuthorityProxy entity);
        Request<AuthorityProxy> add(AuthorityProxy entity);
        Request<AuthorityProxy> createEntity();
        Request<AuthorityProxy> findById(Long id);
        Request<Void> delete(Long id);
        Request<Void> delete(AuthorityProxy entity);
    }

    AuthorityRequest context();
}
