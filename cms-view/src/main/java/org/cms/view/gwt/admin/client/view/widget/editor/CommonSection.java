package org.cms.view.gwt.admin.client.view.widget.editor;


import com.google.gwt.user.client.ui.IsWidget;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public interface CommonSection<T extends CommonProxy> extends IsWidget {

    public T create();

    public void save();

    public void update(T entity);

    public void delete(T entity);

}
