package org.cms.view.gwt.client.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.mvp4g.client.view.ReverseViewInterface;
import org.cms.view.gwt.client.client.presenter.RootPresenter;

public class Root extends Composite implements ReverseViewInterface<RootPresenter>, RootPresenter.RootInterface {

    private RootPresenter presenter;
     FlowPanel  rootPanel = new FlowPanel();

    @Override
    public void setPresenter(RootPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public RootPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void init() {
        rootPanel.add(new Label("Front-End"));
        initWidget(rootPanel);
    }
}