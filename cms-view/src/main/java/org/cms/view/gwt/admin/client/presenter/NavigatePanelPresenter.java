package org.cms.view.gwt.admin.client.presenter;


import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;
import org.cms.view.gwt.admin.client.AdminEventBus;
import org.cms.view.gwt.admin.client.view.container.CommonNavigatePanel;
import org.cms.view.gwt.admin.shared.request.security.SecuritySessionRequestFactory;


@Presenter(view = CommonNavigatePanel.class)
public class NavigatePanelPresenter extends BasePresenter<NavigatePanelPresenter.NavigatePanelInterface, AdminEventBus>{

    private SecuritySessionRequestFactory sessionService;

    public void onStart(){
        sessionService = GWT.create(SecuritySessionRequestFactory.class);
        sessionService.initialize(new SimpleEventBus());
        sessionService.context().getUserRole().fire(new Receiver<String>() {

            @Override
            public void onFailure(ServerFailure error) {
                super.onFailure(error);    //To change body of overridden methods use File | Settings | File Templates.
            }

            @Override
            public void onSuccess(String response) {
                view.init(response.toString());
                getEventBus().changeNavigatePanel(view.asWidget());
            }
        });
    }

    public interface NavigatePanelInterface extends IsWidget{

        public void init(String role);

    }
}
