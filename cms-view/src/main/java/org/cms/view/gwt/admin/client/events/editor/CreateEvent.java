package org.cms.view.gwt.admin.client.events.editor;

import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public class CreateEvent<T extends CommonProxy> extends GwtEvent<CreateEventHandler>{

    public static Type<CreateEventHandler> TYPE = new Type<CreateEventHandler>();

    T entity;

    public CreateEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<CreateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateEventHandler handler) {
        handler.onCreate(this);
    }

    public T getEntity() {
        return entity;
    }
}