package org.cms.view.gwt.admin.shared.proxy.security;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.cms.service.Paging;

import java.util.List;


@ProxyFor(value = Paging.ForUser.class)
public interface UserPagingProxy extends ValueProxy, PagingLoadResult<UserProxy> {
    @Override
    public List<UserProxy> getData();
}