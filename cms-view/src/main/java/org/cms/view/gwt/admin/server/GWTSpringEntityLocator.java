package org.cms.view.gwt.admin.server;

import com.google.web.bindery.requestfactory.shared.Locator;
import org.cms.model.entity.CommonEntity;
import org.hibernate.CacheMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;



@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class GWTSpringEntityLocator<T extends CommonEntity> extends Locator<T, Long> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public T create(Class<? extends T> clazz) {
        try
        {
            return clazz.newInstance();
        } catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public T find(Class<? extends T> clazz, Long id) {
        return (T) sessionFactory.getCurrentSession().createCriteria(clazz).add(Restrictions.eq("id", id)).setCacheable(true).setCacheMode(CacheMode.NORMAL).uniqueResult();
    }
    @Override
    public Class<T> getDomainType() {
        throw new UnsupportedOperationException();
    }
    @Override
    public Long getId(T domainObject) {
        return domainObject.getId();
    }
    @Override
    public Class<Long> getIdType() {
        return Long.class;
    }
    @Override
    public Object getVersion(T domainObject) {
        return domainObject.getVersion();
    }

    @Override
    public boolean isLive(T domainObject) {
        return true;
    }
}