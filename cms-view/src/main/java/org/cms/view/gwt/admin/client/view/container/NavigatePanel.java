package org.cms.view.gwt.admin.client.view.container;


import com.google.gwt.user.client.ui.IsWidget;
import org.cms.view.gwt.admin.client.presenter.NavigatePanelPresenter;


public interface NavigatePanel extends IsWidget {

    public void setPresenter(NavigatePanelPresenter presenter);

    public void init();


}
