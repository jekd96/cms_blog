package org.cms.view.gwt.admin.client.view.container.admin;


import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.view.ReverseViewInterface;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import org.cms.view.gwt.admin.client.presenter.ContentPanelPresenter;

public class ContentAdminPanel extends Composite implements ReverseViewInterface<ContentPanelPresenter>, ContentPanelPresenter.ContentPanelInterface {

    private ContentPanelPresenter presenter;
    ContentPanel contentAdminPanel = new ContentPanel();

    public ContentAdminPanel() {
        contentAdminPanel.setBodyBorder(false);
        contentAdminPanel.setBorders(false);
        contentAdminPanel.setResize(true);
        contentAdminPanel.setHeaderVisible(false);
    }

    @Override
    public void setPresenter(ContentPanelPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ContentPanelPresenter getPresenter() {
        return presenter;
    }

    @Override
    public Widget asWidget() {
        return contentAdminPanel;
    }

    public void loadData(IsWidget section){
        contentAdminPanel.clear();
        contentAdminPanel.add(section.asWidget(), new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        section.asWidget().setHeight(""+contentAdminPanel.getOffsetHeight());
    }
}
