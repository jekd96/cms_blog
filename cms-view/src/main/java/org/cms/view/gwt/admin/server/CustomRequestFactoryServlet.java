package org.cms.view.gwt.admin.server;

import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;

public class CustomRequestFactoryServlet extends RequestFactoryServlet{

    public CustomRequestFactoryServlet() {
        super( new LoquaciousExceptionHandler() );
    }

    public CustomRequestFactoryServlet(LoquaciousExceptionHandler loquaciousExceptionHandler, GWTSpringServiceLayerDecorator gwtSpringServiceLayerDecorator) {
        super(loquaciousExceptionHandler, gwtSpringServiceLayerDecorator);
    }
}
