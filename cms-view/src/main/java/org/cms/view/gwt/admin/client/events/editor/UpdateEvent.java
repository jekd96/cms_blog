package org.cms.view.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.GwtEvent;
import org.cms.view.gwt.admin.shared.proxy.CommonProxy;


public class UpdateEvent<T extends CommonProxy> extends GwtEvent<UpdateEventHandler>{

    public static Type<UpdateEventHandler> TYPE = new Type<UpdateEventHandler>();

    T entity;

    public UpdateEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<UpdateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UpdateEventHandler handler) {
        handler.onUpdate(this);
    }

    public T getEntity() {
        return entity;
    }
}