package org.cms.view.gwt.admin.shared.request.security;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.cms.service.security.UserService;
import org.cms.view.gwt.admin.server.GWTSpringServiceLocator;
import org.cms.view.gwt.admin.shared.proxy.security.UserPagingProxy;
import org.cms.view.gwt.admin.shared.proxy.security.UserProxy;
import org.cms.view.gwt.admin.shared.request.CommonRequestContext;
import org.cms.view.gwt.admin.shared.request.CommonRequestFactory;

import java.util.List;

public interface UserRequestFactory extends CommonRequestFactory {

    @Service(value = UserService.class, locator = GWTSpringServiceLocator.class)
    public interface UserRequest extends CommonRequestContext<UserProxy> {
        Request<UserPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<UserProxy>> list();
        Request<UserProxy> update(UserProxy entity);
        Request<UserProxy> add(UserProxy entity);
        Request<UserProxy> create();
        Request<UserProxy> find(String value);
        Request<Void> delete(Long id);
        Request<Void> delete(UserProxy entity);
    }

    UserRequest context();
}
