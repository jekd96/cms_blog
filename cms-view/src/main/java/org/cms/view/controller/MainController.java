package org.cms.view.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {

    @RequestMapping("/")
    public ModelAndView client(HttpServletRequest req, HttpServletResponse resp){
        return new ModelAndView("client");
    }

    @Secured(value = {"ROLE_ADMIN"})
    @RequestMapping("/admin")
    public ModelAndView admin(HttpServletRequest req, HttpServletResponse resp){
        if(req.isUserInRole("ROLE_ADMIN"))return new ModelAndView("admin");
        return new ModelAndView("login");
    }

    @RequestMapping("/login_admin")
    public ModelAndView loginAdmin(){
        return new ModelAndView("login");
    }

}
